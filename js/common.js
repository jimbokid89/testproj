$(function() {

  var heightWind = $(window).outerHeight();
  var widthWind = $(window).outerWidth();
  var btnLeft = $('.btn').offset().left;
  var btnRight = btnLeft + $('.btn').outerWidth();
  var btnTop = $('.btn').offset().top;
  var btnBottom = btnTop + $('.btn').outerHeight();

  var circle = $('.circle'),
      mx = 2,
      my = 4,
      x = circle.offset().left + 25,
      y = circle.offset().top + 25;

  $(window).bind('load resize ', function() {
    heightWind = $(window).outerHeight();
    widthWind = $(window).outerWidth();
    btnLeft = $('.btn').offset().left;
    btnRight = btnLeft + $('.btn').outerWidth();
    btnTop = $('.btn').offset().top;
    btnBottom = btnTop + $('.btn').outerHeight();
  });

  function circleFn(x, y) {
    circle.css({
      'top': y,
      'left': x,
      'margin': 'initial'
    });
  }

  $('.js-start').on('click', function(e) {
    e.preventDefault();
    if ($(this).hasClass('active')) {
      $(this).html('allready active');
    } else {
      $(this).addClass('active');
      return setInterval(draw, 10);
    }
  })

  function draw() {

    circleFn(x, y);

    if (x + mx > widthWind || x + mx < 0) {
      mx = -mx;
    }

    if (y + my > heightWind || y + my < 0) {
      my = -my;
    }


    if (x > btnLeft && x < btnRight && y > btnTop && y < btnBottom) {
      if (x < btnLeft + 20) {
        console.log('left kick');
        mx = -mx;
      }
      if (x > btnRight - 20) {
        console.log('right kick');
        mx = -mx;
      }

      if (y < btnTop + 5) {
        console.log('top kick');
        my = -my;
      }

      if (y > btnBottom - 5) {
        console.log('bottom kick');
        my = -my;
      }
    }
    x += mx;
    y += my;
  }

});
